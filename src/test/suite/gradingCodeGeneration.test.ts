import * as assert from 'assert';
import { createTestingCode, createGradingCode } from '../../exerciseGrading';


suite('Grading Test Suite', () => {
    test('Testing code generation test', () => {
        let input = {
            "testingData": [
                {
                    "className": "Addition",
                    "functions": [
                        {
                            "functionName": "add",
                            "tests": [
                                {
                                    "testName": "basicAddition",
                                    "input": "1, 2",
                                    "result": "3"
                                },
                                {
                                    "testName": "additionNegative",
                                    "input": "4, -6",
                                    "result": "-2"
                                }
                            ]
                        }
                    ]
                }
            ]
        };
        let expectedResult = `import org.junit.Test;
    import static org.junit.Assert.*;
    
    public class ExerciseTest {Addition instanceOfAddition = new Addition();@Test
                public void basicAddition() {
                    assertEquals(3, instanceOfAddition.add(1, 2));
                }@Test
                public void additionNegative() {
                    assertEquals(-2, instanceOfAddition.add(4, -6));
                }}`;
        let actualResult = createTestingCode(input);

        assert.deepStrictEqual(expectedResult, actualResult);
    });

    test('Grading code generation test', () => {
        let input = {
            "gradingData": [
                {
                    "className": "Addition",
                    "functions": [
                        {
                            "functionName": "add",
                            "tests": [
                                {
                                    "points": 2,
                                    "inputs": [
                                        "1, 2",
                                        "3, 14",
                                        "20, 50"
                                    ],
                                    "resultHash": "d09d15df7284c838548202913be3bebfc12f70ce36e1bcc566c9d0e7f88fabe5"
                                },
                                {
                                    "points": 1,
                                    "inputs": [
                                        "32, -2",
                                        "13, -11",
                                        "83, -50"
                                    ],
                                    "resultHash": "fd0ebc9d11a2bb40f812b161fcc1163b3ce111f751c149e689772c1799c62c8f"
                                }
                            ]
                        }
                    ]
                }
            ]
        };
        let expectedResult = {
            gradeCode: `public class GradeExercise {Addition instanceOfAddition = new Addition();public void func0d09d15df7284c838548202913be3bebfc12f70ce36e1bcc566c9d0e7f88fabe5() {System.out.print(instanceOfAddition.add(1, 2));System.out.print(instanceOfAddition.add(3, 14));System.out.print(instanceOfAddition.add(20, 50));}public void func1fd0ebc9d11a2bb40f812b161fcc1163b3ce111f751c149e689772c1799c62c8f() {System.out.print(instanceOfAddition.add(32, -2));System.out.print(instanceOfAddition.add(13, -11));System.out.print(instanceOfAddition.add(83, -50));}public static void main(String[] args) {
        GradeExercise grader = new GradeExercise();
        switch (args[0]) {case "0d09d15df7284c838548202913be3bebfc12f70ce36e1bcc566c9d0e7f88fabe5":
        grader.func0d09d15df7284c838548202913be3bebfc12f70ce36e1bcc566c9d0e7f88fabe5();
        break;case "1fd0ebc9d11a2bb40f812b161fcc1163b3ce111f751c149e689772c1799c62c8f":
        grader.func1fd0ebc9d11a2bb40f812b161fcc1163b3ce111f751c149e689772c1799c62c8f();
        break;}}}`,
            tests: [{
                class: "Addition",
                function: "add",
                points: 2,
                resultHash: "d09d15df7284c838548202913be3bebfc12f70ce36e1bcc566c9d0e7f88fabe5",
                testNumber: 0
            }, {


                class: "Addition",
                function: "add",
                points: 1,
                resultHash: "fd0ebc9d11a2bb40f812b161fcc1163b3ce111f751c149e689772c1799c62c8f",
                testNumber: 1
            }]
        };
        let actualResult = createTestingCode(input);

        assert.deepStrictEqual(expectedResult, actualResult);
    });
});
