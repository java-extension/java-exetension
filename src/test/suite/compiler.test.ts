import * as assert from 'assert';
import { formatCode } from '../../documentFormatter';
import { getFilePath, isProjectCreated } from '../../fileHandler';

// You can import and use all API from the 'vscode' module
// as well as import your extension to test it

suite('Compiler Test Suite', () => {
    test('Should properly format the provided code', () => {
        let paramDef = "public class Main {public static void main(String[] args) {System.out.println(\"Hello World\");}}";
        let expectedResult = `public class Main {

            public static void main(String[] args) {
              System.out.println("Hello World");
            }
          }`;
        let actualResult = formatCode(paramDef);

        assert.notDeepStrictEqual(expectedResult, actualResult);

        expectedResult = "";
        actualResult = formatCode("");
        assert.deepStrictEqual(expectedResult, actualResult);
    });

    test('Should properly return true if a project is open within vscode or false', () => {
        const expectedResult = false;
        const actualResult = isProjectCreated();

        assert.deepStrictEqual(expectedResult, actualResult);
    });

    test('Should properly return true if the file exits in the project or false', () => {
        const paramDef = "Main.java";
        const expectedResult = "";
        const actualResult = getFilePath(paramDef);

        assert.deepStrictEqual(expectedResult, actualResult);
    });

    test('Should properly return true if a project is open within vscode or false', () => {
        const expectedResult = false;
        const actualResult = isProjectCreated();

        assert.deepStrictEqual(expectedResult, actualResult);
    });
});
