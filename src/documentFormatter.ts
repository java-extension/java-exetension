const prettier = require("prettier");
import * as vscode from 'vscode';

// Formats the java code
export let codeFormatter = () => {
  let textEditor = vscode.window.activeTextEditor;
  let languageid = textEditor?.document.languageId;

  if (!textEditor || languageid !== "java") { return; };

  const unformatterCode = textEditor.document.getText();

  const formattedCode = formatCode(unformatterCode);

  const firstLine = textEditor.document.lineAt(0);
  const lastLine = textEditor.document.lineAt(textEditor.document.lineCount - 1);
  const textRange = new vscode.Range(firstLine.range.start, lastLine.range.end);

  textEditor.edit(builder => builder.replace(textRange, formattedCode));
  textEditor.document.save();
};

export let formatCode = (unformatterCode: string) => {
  return prettier.format(unformatterCode, {
    parser: "java",
    tabWidth: 2
  });
};
