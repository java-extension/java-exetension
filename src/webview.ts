// @ts-nocheck
import * as vscode from 'vscode'; // vscode api

// Creates and shows web view panel
export function getWebviewPanel(moduleCode: string) {
  const panel = vscode.window.createWebviewPanel(moduleCode, moduleCode, vscode.ViewColumn.Beside, {
    // Enable scripts in the webview
    enableScripts: true,
    retainContextWhenHidden: true,
    enableCommandUris: true
  }
  );

  // And set its HTML content
  panel.webview.html = "<h3><a class=\"link\" href=\"command:java.learning\">Find an excercise</a></h3>";

  return panel;
}

// Creates content for the web view panel
export function getWebviewContent(extensionUri: vscode.Uri, panel: vscode.WebviewPanel, htmlContent: string) {

  // Local path to css styles and js
  const stylesResetUri = panel.webview.asWebviewUri(vscode.Uri.joinPath(extensionUri, 'media', 'reset.css'));
  const stylesMainUri = panel.webview.asWebviewUri(vscode.Uri.joinPath(extensionUri, 'media', 'vscode.css'));
  const mainStyle = panel.webview.asWebviewUri(vscode.Uri.joinPath(extensionUri, 'media', 'style.css'));
  const scriptUri = panel.webview.asWebviewUri(vscode.Uri.joinPath(extensionUri, 'media', 'main.js'));

  let htmlTestButton: string = htmlContent.testing.testingData.length > 0 ? `<a class="link" href="command:java.runTesting">Run Test</a><br><br>` : "";
  let htmlGradeButton: string = htmlContent.testing.gradingData.length > 0 ? `<a class="link" href="command:java.runGrading">Run Grading</a><br><br>` : "";
  let webviewContent = `
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link href="${stylesResetUri}" rel="stylesheet">
      <link href="${stylesMainUri}" rel="stylesheet">
      <link href="${mainStyle}" rel="stylesheet">
  </head>
  <body>
      <div id="app">
    <h1 class"title">${htmlContent.label}: ${htmlContent.detail}</h1><br>
    <p>${htmlContent.body}</p>
    </div>
    <br>
    ${htmlTestButton}
    ${htmlGradeButton}
    <a class="link" href="command:java.learning">Next Excercise</a>
    <!-- <script src="${scriptUri}"></script> -->
  </body>
  </html>`;

  return webviewContent;
}