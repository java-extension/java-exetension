import * as vscode from 'vscode'; // vscode api
import fs = require('fs');
import path = require('path');
import { formatCode } from './documentFormatter';


// If workspace is empty or no project has been opened return false, otherwise true
export let isProjectCreated = () => {
    if (!vscode.workspace.workspaceFolders) {
        return false;
    } else {
        return true;
    }
};

// Returns full file path
export let getFilePath = (fileName: string) => {
    if (vscode.workspace.workspaceFolders) {
        let folderPath = vscode.workspace.workspaceFolders[0].uri.toString().split(":")[1];
        return path.join(folderPath, fileName);
    } else {
        vscode.window.showErrorMessage("Workspace is empty. Please create/open a project");
        return "";
    }
};

// Check if the file is already created previously
export let isFileExist = (fileName: string) => {
    if (!fs.existsSync(fileName)) {
        return true; // The excercise file already created 
    } else {
        return false;
    }
};

// Creates an exercise file and adds contents to the file
export let createExerciseFile = (filePath: string, content: string) => {
    fs.writeFile(filePath, formatCode(content), err => {
        if (err) {
            vscode.window.showErrorMessage(err.toString());
            return;
        }
    });
};

// Opens the exercise file
export let openExerciseFile = (filePath: string) => {
    if (!filePath) { return; };
    if (vscode.workspace.workspaceFolders) {
        const openPath = vscode.Uri.file(filePath);
        vscode.workspace.openTextDocument(openPath).then(doc => {
            vscode.window.showTextDocument(doc, vscode.ViewColumn.Two, false);
            // .then(e => {
            //   e.edit(edit => {
            //     // edit.insert(new vscode.Position(0, 0), "\n"); // this is a simple hack to open the doc fully(no preview mode)
            //   })
            // });
        });
    }
};

// Creates a new directory if doesn't exits one
export let createDirectory = (dirName: string) => {
    try {
        if (!fs.existsSync(dirName)) {
            fs.mkdirSync(dirName, { recursive: true });
        }
        return dirName;
    } catch (err) {
        console.error(err);
    }
};

// Creates a new file and adds the given content to it
export let createFile = (filename: string, content: string) => {
    const filePath = getFilePath(filename);
    if (fs.existsSync(filePath)) {
        return; // The excercise file already xists 
    }
    fs.writeFile(filePath, content, err => {
        if (err) {
            vscode.window.showErrorMessage("Content could not be added to the file");
            return;
        }
    });
};
