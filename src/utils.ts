import * as vscode from "vscode";
export async function fileExists(name: string) {
  let glob = `**/${name}`;
  return await vscode.workspace.findFiles(glob, "").then(function (uris) {
    return uris.length > 0;
  });
}

export function clear(terminal: vscode.Terminal) {
  if (process.platform === "win32") {
    terminal.sendText("cls", true);
  } else {
    terminal.sendText("clear", true);
  }
}

export function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}