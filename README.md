# LeedsJava

## Quick start

1. Download the extension in VSIX format from [here](https://drive.google.com/file/d/1XqaY8uhCu0_yvqsCSdxBRVsKn7ncWl7k/view?usp=sharing). Download the entire VSIX file.
2. Make sure you have [JDK](https://www.oracle.com/java/technologies/javase-downloads.html), [Git](https://git-scm.com/downloads) and optionally [Gradle](https://gradle.org/install/) installed
3. Run `code --install-extension extension_name.vsix` to install the extension

## Features

- GUI for running and testing your application, with Gradle support
- Gradle support
- Assistance with GitLab onboarding
- Coursework delivery with grading
- Java code snippets
- Java code formatter
- PMD Code analysis 
- PMD Code style checking

## Usage

### Running, building and testing
Use the titlebar buttons to build, run and test your application.
The extension will automatically detect if you're using Gradle or not.
To run the application with arguments, open the command palette (`Ctrl+Shift+P`, `Cmd+Shift+P` on Mac) and run the `Set program arguments` command.
Enter the desired arguments in the prompt. The arguments will be remembered for the workspace and can be changed by running the above command or in the settings.

![buttons](assets/readme/buttons.png)

### GitLab onboarding
The extension will automatically prompt you for your name and email, configure Git and generate SSH keys for GitLab if none were detected.

You can reconfigure Git and generate new SSH keys by opening the command palette (`Ctrl+Shift+P`, `Cmd+Shift+P` on Mac) and running the `Change credentials` and `Generate new SSH key` commands respectively.

Configuration detection on startup can be disabled in settings.

### How to use Java Code Snippets
- The extension uses Intellij Idea like code snippets e.g.
'fori' will produce a for loop snippets whose values can modified using 'Tab' button from your keyboard.
![snippets](assets/readme/snippets.png)

### Format your code automatically
- Right click on any .java file extension and choose 'Java Format Document' option to format your code automatically.
![format](assets/readme/format.png)

### PMD Code Analysis tool
- Right click on any .java file extension and choose 'Java Run Code Analysis' option to analyse your Java code. The report (in HTML form) will generated in your-project-dir/.pmd-analysis-code/java_best_practices_report.html
![analysis](assets/readme/analysis.png)

- The code analysis report can be viewed on your internet browser.

### Code StyleCheck tool
- Right click on any .java file extension and choose 'Java Run Checkstyle' option to check code style. The report (in HTML form) will generated in your-project-dir/.pmd-analysis-code/checkstyle_report.html
![stylecheck](assets/readme/stylecheck.png)

- The code analysis report can be viewed on your internet browser.

### Coursework delivery
#### For students
- Create/Open a folder in vscode.
- You must have the coursework link provided by your lecturer.
- Paste the link in VS Code settings into `Java: Learning API` field
  ![link](assets/readme/link.png)

- Close settings and any opened tabs if active
- Open the command palette (`Ctrl+Shift+P`, `Cmd+Shift+P` on Mac) and run the `COMP1721: Excercises` command
![link](assets/readme/pallet.png)
- Click on `Find an exercise` and choose an exercise
![link](assets/readme/exercises.png)
- Follow the on screen instructions
![exercise](assets/readme/exercise.png)
- Once you have solved your exercise, test it to make sure it's correct
![testing](assets/readme/testing.png)
- When you're ready, grade your exercise. This is only done locally and grading will be ran again by your lecturer after you submit your coursework.
![grading](assets/readme/grading.png)

#### For lecturers
Coursework data is defined in a JSON file. It contains instructions for the exercise, an optional template file for the student, optional testing data with inputs and outputs for the tested functions and optional grading data with inputs, hash of concatenated outputs and number of points to be earned for the tested functions. An example is available below. 
```
[
	{
		"excerciseNumber": 1,
		"title": "Addition",
		"description": "<p>In <code>Addition.java</code> Create a method called <code>add</code> that takes two integers and returns their sum</p>",
		"exerciseContent": {
			"contentProvided": true,
			"javaFileName": "Addition.java",
			"javaContent": "public class Addition{}"
		},
		"testing": {
			"testingData": [
				{
					"className": "Addition",
					"functions": [
						{
							"functionName": "add",
							"tests": [
								{
									"testName": "basicAddition",
									"input": "1, 2",
									"result": "3"
								},
								{
									"testName": "additionNegative",
									"input": "4, -6",
									"result": "-2"
								}
							]
						}
					]
				}
			],
			"gradingData": [
				{
					"className": "Addition",
					"functions": [
						{
							"functionName": "add",
							"tests": [
								{
									"points": 2,
									"inputs": [
										"1, 2",
										"3, 14",
										"20, 50"
									],
									"resultHash": "11204807452a2b82ab255d10b78d86de070bef445237b43bdaadf1b09d1f385b9271c38762a051543c6818db694ae1435ae711cb28c02b52f371ef2dfaed51a6"
								},
								{
									"points": 1,
									"inputs": [
										"32, -2",
										"13, -11",
										"83, -50"
									],
									"resultHash": "7c0343121fe04464c1ecd405d71e8c644d4fd539d950e4440fefb1bc8845e44592f9b4df960d07b6d663396bfe15c8fc103a808d3f663b4deebb895e04908f4e"
								}
							]
						}
					]
				}
			]
		}
	}
]
```
In grading, each test contains a list of inputs and a hash of the concatenated answers to prevent the students from being able to see obtain the outputs from the coursework file. When creating a coursework file, to obtain the hash, write the tests, enter arbitrary string in the `resultHash` field and run grading. The expected and actual hashes will be displayed in the output.
When finished, provide your students with the link to the raw JSON file.

## Running from source

- Clone the repository and open it in VS Code
- Run `npm install` to install dependencies
- Press F5 to open a new VS Code window with the extension running

## Attribution
Icons copyright belongs to Microsoft Corporation, licensed under Creative Commons Attribution 4.0 International license.
https://github.com/microsoft/vscode-codicons

PMD Checkstyle & Code Analysis copyright belongs to InfoEther, LLC, Licensed under the Apache License, Version 2.0
https://github.com/pmd/pmd